from typing import List
import random


from fastapi import FastAPI, Path
from sqlalchemy.future import select

import models
import schemas
from database import engine, session


app = FastAPI()


def ingredients() -> str:
    ingredients = ''
    for _ in range(1, 6):
        ingredients += f'ingredient{random.randint(1, 20)}, '
    return ingredients


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)
    async with session.begin():
        for index in range(10):
            recipe = models.Recipe(
                title=f'recipe{index}',
                time_of_cooking=random.randint(5, 15),
                ingredients=ingredients(),
                text_description='This page will be updated soon')
            session.add(recipe)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get('/recipes/', response_model=List[schemas.RecipeOut])
async def recipes() -> List[models.Recipe]:
    res = await session.execute(select(models.Recipe))
    list_of_recipes = res.scalars().all()
    print(list_of_recipes[2].views)
    length = len(list_of_recipes)
    for i in range(length):
        for j in range(1, length - i):
            if list_of_recipes[j - 1].views < list_of_recipes[j].views:
                list_of_recipes[j - 1], list_of_recipes[j] = list_of_recipes[j], list_of_recipes[j - 1]
            elif list_of_recipes[j - 1].views == list_of_recipes[j].views:
                if list_of_recipes[j - 1].time_of_cooking > list_of_recipes[j].time_of_cooking:
                    list_of_recipes[j - 1], list_of_recipes[j] = list_of_recipes[j], list_of_recipes[j - 1]
            elif list_of_recipes[j - 1].views == list_of_recipes[j].views and \
                    list_of_recipes[j - 1].time_of_cooking == list_of_recipes[j].time_of_cooking:
                if list_of_recipes[j - 1].title > list_of_recipes[j].title:
                    list_of_recipes[j - 1], list_of_recipes[j] = list_of_recipes[j], list_of_recipes[j - 1]
    return list_of_recipes


@app.get('/recipes/{id_}', response_model=schemas.RecipeOut)
async def recipe_id(
        id_: int = Path(
            ...,
            ge=0
        )
) -> models.Recipe:
    res = await session.execute(select(models.Recipe).filter(models.Recipe.id == id_))
    recipe = res.scalars().one()
    recipe.views += 1
    return recipe
