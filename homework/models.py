from sqlalchemy import Column, String, Integer, ARRAY

from database import Base

class Recipe(Base):
    __tablename__ = 'Recipe'
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    time_of_cooking = Column(Integer, nullable=False)
    views = Column(Integer, nullable=False, default=0)
    ingredients = Column(String, nullable=False)
    text_description = Column(String, nullable=False)

