from typing import List

from pydantic import BaseModel


class Recipe(BaseModel):
    title: str
    time_of_cooking: int
    views: int
    ingredients: str
    text_description: str


class RecipeOut(Recipe):
    id: int

    class Config:
        orm_mode = True
